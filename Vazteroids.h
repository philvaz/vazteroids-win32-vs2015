// Vazteroids.h by PhilVaz
// June 12, 2003

// HEADER FILE ///////////////////////////////////////////////

// SOUNDS

#define SOUND_FIRE1      101
#define SOUND_FIRE2      102
#define SOUND_EXPLODE1   103
#define SOUND_EXPLODE2   104
#define SOUND_EXPLODE3   105
#define SOUND_EXPLODE4   106
#define SOUND_EXPLODE5   107
#define SOUND_REGEN1     108
#define SOUND_REGEN2     109
#define SOUND_THRUST     110
#define SOUND_UFO1       111
#define SOUND_UFO2       112
#define SOUND_EXTRA      113
